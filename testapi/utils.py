# import os
from rest_framework.exceptions import ValidationError
import aiohttp
from .models import *
from django.utils.timezone import make_aware
from datetime import datetime
from dotenv import dotenv_values


config = dotenv_values(".env")

async def notification():
    headers = {
        'Authorization': f'Bearer {config["auth_token"]}',
    }
    url = config['api_url']
    aware_datetime = make_aware(datetime.now())
    async for sending in Sending.objects.filter(start_time__lte=aware_datetime, end_time__gte=aware_datetime):
        async for client in Client.objects.filter(tag=sending.tag, code=sending.code):
            message_obj = Message.objects.filter(sending_id=sending.id, client_id=client.id)
            if await message_obj.acount() == 0:
                message_obj = Message(sending_id=sending.id, client_id=client.id)
                await message_obj.asave()
                last_message = await Message.objects.all().alast()
                id = last_message.id
                payload = {"id": id, "phone": client.phone, "text": sending.message_text}
                res = await post_data(f"{url}{id}", payload, headers)
                if res['message'] == 'OK':
                    message_obj.status = True
                    await message_obj.asave()
            else:
                async for item in message_obj:
                    if not item.status:
                        payload = {"id": item.id, "phone": client.phone, "text": sending.message_text}
                        res = await post_data(f"{url}{item.id}", payload, headers)
                        if res['message'] == 'OK':
                            item.status = True
                            await item.asave()

async def post_data(url, payload, headers):
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.post(url, json=payload) as resp:
            if resp.status == 200:
                return await resp.json()
            else:
                return {'code': -1, 'message': await resp.text()}

def check_phone_format(request):
    phone = request.data.get('phone', False)
    if phone:
        if not str(phone).isdigit() or len(str(phone)) != 11:
            raise ValidationError('номер телефона клиента должен быть в формате 7XXXXXXXXXX (X - цифра от 0 до 9)')

