from django.http import HttpResponseNotFound, HttpResponse, JsonResponse
from rest_framework import status
from rest_framework.response import Response


def handler_404(request, *args, **kwargs):
    return JsonResponse({'detail': 'Page not found (404)'}, status=404)

def handler_500(request, *args, **kwargs):
    return JsonResponse({'detail': 'Server Error (500)'}, status=500)

# def handler_500(request, *args, **kwargs):
#     return Response({'detail': 'Server Error (500)'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)