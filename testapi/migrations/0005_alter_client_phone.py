# Generated by Django 4.2.1 on 2023-05-08 03:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('testapi', '0004_client_user_message_user_sending_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='phone',
            field=models.BigIntegerField(),
        ),
    ]
