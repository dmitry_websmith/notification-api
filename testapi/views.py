from django.forms import model_to_dict
from django.shortcuts import redirect
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import *
from .utils import *


def index(request):
    return redirect('schema-swagger-ui')

class ClientAPIList(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        check_phone_format(self.request)
        serializer.save()

class ClientAPIDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def perform_update(self, serializer):
        check_phone_format(self.request)
        serializer.save()

# рассылка
class SendingAPIList(generics.ListCreateAPIView):
    queryset = Sending.objects.all()
    serializer_class = SendingSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    # @swagger_auto_schema(tags=['Sending'])
    # def get(self, *args, **kwargs):
    #     return super().get(self, *args, **kwargs)

class SendingAPICommonStats(APIView):
    # получение общей статистики по созданным рассылкам и количеству отправленных сообщений по ним с группировкой
    # по статусам
    def get(self, *args, **kwargs):
        data_dict = {}
        sending_obj = Sending.objects.all()
        data_dict['sending_count'] = sending_obj.count()
        data_dict['successed'] = 0
        data_dict['failed'] = 0
        for item in sending_obj:
            # количество успешно отправленных сообщений этой рассылки (status = True)
            data_dict['successed'] = data_dict['successed'] + \
                                              Message.objects.filter(sending_id=item.id, status=True).count()
            # количество безуспешно отправленных сообщений этой рассылки (status = False)
            data_dict['failed'] = data_dict['failed'] + \
                                           Message.objects.filter(sending_id=item.id, status=False).count()
        return Response([data_dict], status=status.HTTP_200_OK)

class SendingAPIDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Sending.objects.all()
    serializer_class = SendingSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

class SendingAPIDetailStats(APIView):
    # получения детальной статистики отправленных сообщений по конкретной рассылке
    def get(self, *args, **kwargs):
        pk = kwargs.get("pk", None)
        if Sending.objects.filter(pk=pk).count() == 0:
            raise ValidationError({"detail": "Not found."})
        data_dict = model_to_dict(Sending.objects.get(pk=pk))
        # количество успешно отправленных сообщений этой рассылки (status = True)
        data_dict['successed_messages'] = Message.objects.filter(sending_id=pk, status=True).count()
        # количество безуспешно отправленных сообщений этой рассылки (status = False)
        data_dict['failed_messages'] = Message.objects.filter(sending_id=pk, status=False).count()
        data_dict['detail'] = list(Message.objects.filter(sending_id=pk).values(
            'client', 'status'))
        return Response([data_dict], status=status.HTTP_200_OK)

# сообщение
class MessageAPIList(generics.ListCreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

class MessageAPIDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)