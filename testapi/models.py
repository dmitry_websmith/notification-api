from django.db import models
from django.contrib.auth.models import User


# Сущность "клиент"
class Client(models.Model):
    # номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)
    phone = models.BigIntegerField()
    # код мобильного оператора
    code = models.IntegerField()
    # тег (произвольная метка)
    tag = models.CharField(max_length=512, default="")
    # часовой пояс
    timezone = models.CharField(max_length=512, default="UTC 0")

    def __str__(self):
        return str(self.phone)

# Сущность "рассылка"
class Sending(models.Model):
    # дата и время запуска рассылки
    start_time = models.DateTimeField()
    # дата и время окончания рассылки: если по каким-то причинам не успели разослать все сообщения -
    # никакие сообщения клиентам после этого времени доставляться не должны
    end_time = models.DateTimeField()
    # текст сообщения для доставки клиенту
    message_text = models.TextField()
    # фильтр свойств клиентов, на которых должна быть произведена рассылка (код мобильного оператора, тег)
    tag = models.CharField(max_length=512, default="")
    code = models.IntegerField()

    def __str__(self):
        return str(self.pk)

# Сущность "сообщение"
class Message(models.Model):
    # дата и время создания (отправки)
    update_time = models.DateTimeField(auto_now_add=True) # time of creation
    # статус отправки
    status = models.BooleanField(default=False)
    # id рассылки, в рамках которой было отправлено сообщение
    sending = models.ForeignKey(Sending, on_delete=models.CASCADE)
    # id клиента, которому отправили
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
