from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('testapi.urls')),
]

handler404 = "craft.views.handler_404"
handler500 = "craft.views.handler_500"
