from .views import *
from django.urls import path, re_path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework.authtoken.views import obtain_auth_token


schema_view = get_schema_view(
    openapi.Info(
        title="Notification Service",
        default_version='v1',
        description="Test task for developer candidates",
        terms_of_service="https://www.open-data.su",
        contact=openapi.Contact(email="websmith@inbox.ru"),
        license=openapi.License(name="License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny, ],
)

urlpatterns = [
    re_path(r'^doc(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('doc/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('', index, name='home'),
    path('api/v1/clientlist/', ClientAPIList.as_view()),
    path('api/v1/client/<int:pk>/', ClientAPIDetail.as_view()),
    path('api/v1/sendinglist/', SendingAPIList.as_view()),
    path('api/v1/sending/stats/', SendingAPICommonStats.as_view()),
    path('api/v1/sending/<int:pk>/', SendingAPIDetail.as_view()),
    path('api/v1/sending/stats/<int:pk>/', SendingAPIDetailStats.as_view()),
    path('api/v1/messagelist/', MessageAPIList.as_view()),
    path('api/v1/message/<int:pk>/', MessageAPIDetail.as_view()),
    path('token/', obtain_auth_token),
    path('accounts/', include('rest_framework.urls'))
]






